package com.ideas.ngimocker.components;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

import static java.net.http.HttpRequest.newBuilder;

@Component
public class HTTPClientWrapper {
    Logger logger = LoggerFactory.getLogger(this.getClass());
    private final HttpClient httpClient = HttpClient.newHttpClient();

    public String makeGetRequest(String uri) {
        return makeRequest(uri, "GET", HttpRequest.BodyPublishers.noBody());
    }

    public String makePostRequest(String uri, HttpRequest.BodyPublisher bodyPublisher) {
        return makeRequest(uri, "POST", bodyPublisher);
    }
    public String makePostRequest(String uri, String data, String[] headers) throws IOException, InterruptedException {
        // return makeRequest(uri, "POST", bodyPublisher);
        HttpClient client = HttpClient.newBuilder().build();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(uri))
                .headers(headers)
                .POST(HttpRequest.BodyPublishers.ofString(data))
                .build();

        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
        return response.body();
    }

    private String makeRequest(String uri, String method, HttpRequest.BodyPublisher body) {
        try {
            logger.info("Request " + method + " " + uri);
            HttpResponse<String> response = httpClient.send(
                    newBuilder(URI.create(uri)).method(method, body).build(),
                    HttpResponse.BodyHandlers.ofString());
            if (response.statusCode() >= 400) {
                String msg = "Unrecognized response from server. " + response.statusCode() + " for url " + uri;
                throw new RuntimeException(msg);
            }
            return response.body();
        } catch (IOException | InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
